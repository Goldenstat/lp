<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/png" href="{{asset('img/logo-square.png')}}">

    <title>Leverson Palmer Estate Agents</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/7.0.2/css/bootstrap-slider.min.css">
    <link href="//www.fuelcdn.com/fuelux/3.13.0/css/fuelux.min.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout" class="fuelux">


    <nav class="navbar navbar-default">
        <div class="container-fluid nav-container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('img/logo-square.png')}}" alt="" class="img-responsive logo">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav links">
                    <li><a href="{{ route('properties.index') }}">Sales</a></li>
                    <li><a href="/about">Why choose Us</a></li>
                    <li><a href="/contact">Contact Us</a></li>
                    <li><a href="{{route('review.index')}}">Client Reviews</a></li>
                    <li><a href="/search">Search</a></li>
                </ul>
                <!-- right side of navbar -->
                <ul class="nav-contact">
                    <li>Contact Us</li>
                    <li>Email: sales@leversonpalmer.com</li>
                    <li>Phone: 01509 735218</li>
                    <li><!--First 100 Properties &pound; 500!--></li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <div class="container skyline @if(Route::current()->getName() == 'index' || Route::current()->getName() == 'properties.create' || Route::current()->getName() == 'properties.show' || Route::current()->getName() == 'rooms.create' || Route::current()->getName() == 'properties.edit' || Route::current()->getName() == 'review.pending') footer-home @else footer @endif">
        <div class="container">
            <p class="footer-tagline">
                Contact us today - 01509 735218
            </p>
            <p class="copyright">
                Website design Copyright &copy; Robert Dunne, Content Copyright &copy; Leverson Palmer.
                <br>
                <a href="#">Terms</a> | <a href="#">Privacy</a>
            </p>
        </div>
    </div>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/7.0.2/bootstrap-slider.min.js"></script>
    <script src="//www.fuelcdn.com/fuelux/3.13.0/js/fuelux.min.js"></script>
    <script src="{{ asset('js/all.js') }}"></script>
    @yield('scripts')

    @include('layouts.flash')

    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
