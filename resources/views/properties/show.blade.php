@extends('layouts.app')

@section('content')

    <div class="container">
        @if(auth()->check())
            @if($property->rightMove == '' && !is_null($property->photos()))
                <a href="{{route('properties.rm', ['id' => $property->id])}}" class="btn btn-primary btn-lg">Send to RightMove</a>
            @else
                <a href="{{route('properties.rrm', ['id' => $property->id])}}" class="btn btn-danger btn-lg pull-right">Remove from Rightmove</a>
            @endif
        @endif
        <div class="row">
            <div class="col-md-4">
                    <div class="pull-right admin-controls">
                    @if(auth()->check())
                        <a href="{{route('properties.edit', ['id' => $property->id])}}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                        <form action="{{route('properties.destroy', ['id' => $property->id])}}" method="POST">
                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="delete">
                            <button type="submit" class="btn btn-danger"><i class="fa fa-times fa-fw"></i> Delete</button>
                        </form>
                    @elseif($property->rightMove != '')
                        <a href="{{$property->rightMove}}" class="btn btn-primary" style="margin-top: 0.5em;">View on right move</a>
                    @endif
                    </div>
                <h1>{!! $property->number !!} {!! $property->street!!}</h1>

                <h2 class="page-heading">&pound; {!! number_format($property->price) !!}</h2>

                <div class="description">
                    {!! nl2br($property->description) !!}
                </div>
            </div>

            <div class="col-md-8 property-disp-img">
                @foreach($property->photos->chunk(4) as $set)
                    <div class="row">
                        @foreach($set as $photo)
                            <div class="col-md-3">
                                @if(auth()->check())
                                    <form method="post" action="/photos/{{$photo->id}}">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE">

                                        <button class="btn btn-primary" type="submit"><i class="fa fa-times"></i></button>
                                    </form>
                                @endif
                                <a href="{{asset($photo->path)}}" data-lightbox="gallery">
                                    <img src="{{asset($photo->thumbnail_path)}}" />
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endforeach

                @if(auth()->check())
                <hr>

                <h2>Add Your Photos</h2>

                <form id="addPhotosForm" action="/properties/{{$property->id}}/photos" method="POST" class="dropzone">
                    {!! csrf_field() !!}
                </form>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                @foreach($property->rooms->chunk(2) as $chunk)
                    <div class="row">
                        @foreach($chunk as $room)
                            <div class="col-md-4">
                                <h5>{{$room->name}}</h5>
                                <p>{{$room->desc}}</p>
                                <div>
                                    <p>Width: {{$room->width}} {{$room->unit}}</p>
                                    <p>Length: {{$room->length}} {{$room->unit}}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script>
        Dropzone.options.addPhotosForm = {
            paramName: 'photo',
            maxFileSize: 10,
            acceptedFiles: '.jpg, .jpeg, .png, .bmp'
        };
    </script>
@endsection