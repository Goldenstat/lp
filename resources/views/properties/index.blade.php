@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <h2 class="page-heading">Properties for sale</h2>
    @forelse($properties->chunk(4) as $chunk)
    <div class="row full-width-white">
        <div class="container">
            @foreach($chunk as $property)
                <div class="property-box col-md-3">
                    <a href="/properties/{{$property->id}}">
                        <div class="bg-fade">
                            <img src="../{!! $property->photos->first()['path'] !!}" alt="" class="fade-img property-img">
                        </div>
                        <p class="property-box-rd">{{$property->street}} {{$property->post}}</p>
                        <p class="property-box-price">&pound; {{number_format($property->price) }}</p>
                    </a>
                    <a href="#" class="btn btn-sm btn-default">{{$property->state}}</a>
                    @if($property->rightMove != '')
                        <a href="{{$property->rightMove}}" class="btn btn-primary pull-right">View on RightMove</a>
                    @endif

                </div>
            @endforeach
        </div>
    </div>
    @empty
        <div style="height: 330px">
            <h2 class="text-center">There are no properties found, please refine your search</h2>
        </div>
    @endforelse
    </div>
@stop
