@extends('layouts.app')

@section('content')

    <div class="container">
        <h1 class="page-heading">Edit {{$property->street}}</h1>

        <form action="{{route('properties.update', ['id' => $property->id])}}" method="post">
            <input type="hidden" name="_method" value="PATCH">
            <div class="row">
                <div class="col-md-6">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="number">House number: </label>
                        <input type="number" name="number" id="number" class="form-control" value="{{old('number', $property->number)}}" required>
                    </div>

                    <div class="form-group">
                        <label for="street">Street: </label>
                        <input type="text" name="street" id="street" class="form-control" value="{{old('street', $property->street)}}" required>
                    </div>

                    <div class="form-group">
                        <label for="city">Town: </label>
                        <input type="text" name="town" id="town" class="form-control" value="{{old('town', $property->town)}}" required>
                    </div>

                    <div class="form-group">
                        <label for="code">Post Code (1st half): </label>
                        <input type="text" name="code1" id="code1" class="form-control" value="{{old('code1', $property->code1)}}" required>
                    </div>
                    <div class="form-group">
                        <label for="code">Post Code (2nd half): </label>
                        <input type="text" name="code2" id="code2" class="form-control" value="{{old('code2', $property->code2)}}" required>
                    </div>

                    <div class="form-group">
                        <label for="bedrooms">Number of Bedrooms: </label>
                        <input type="number" name="bedrooms" id="bedrooms" class="form-control" value="{{old('bedrooms', $property->bedrooms)}}" required>
                    </div>
                    <div class="form-group">
                        <label for="reception">Reception Rooms: </label>
                        <input type="number" name="reception" id="reception" class="form-control" value="{{old('reception', $property->reception)}}" required>
                    </div>
                    <div class="form-group">
                        <label for="state">State: </label>
                        <select name="state" id="state" class="form-control" required>
                            <option value="Available">For Sale</option>
                            <option value="UnderOffer">Under Offer</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="gardens">Gardens: </label>
                        <input type="number" name="gardens" id="gardens" class="form-control" value="{{old('gardens', $property->gardens)}}" required>
                    </div>
                    <div class="form-group">
                        <label for="parking">Parking: </label>
                        <select name="parking" id="parking" class="form-control" required>
                            <option value="Allocated">Allocated</option>
                            <option value="Communal">Communal</option>
                            <option value="Covered">Covered</option>
                            <option value="Driveway">Driveway</option>
                            <option value="Garage">Garage</option>
                            <option value="Gated">Gated</option>
                            <option value="Offstreet">Off Street</option>
                            <option value="Onstreet">On Street</option>
                            <option value="Permit">Permit</option>
                            <option value="PrivateParking">Private</option>
                            <option value="Rear">Rear</option>
                            <option value="Residents">Residents</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="type">Type: </label>
                        <select name="type" id="type" class="form-control" required>
                            <option value="DetachedHouse">Detached</option>
                            <option value="SemiDetachedHouse">Semi Detached</option>
                            <option value="TerracedHouse">Terraced</option>
                            <option value="Flat">Flat</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="price">Sale Price: (&pound;)</label>
                        <input type="number" name="price" id="price" class="form-control" value="{{old('price', $property->price)}}" required>
                    </div>

                    <div class="form-group">
                        <label for="description">Description: </label>
                        <textarea name="description" id="description" class="form-control" rows="10" required>
                            {{old('description', $property->description)}}
                        </textarea>
                    </div>

                    <div class="form-group">
                        <label for="rightMove" class="control-label">Rightmove URL</label>
                        <input type="text" name="rightMove" class="form-control" id="rightMove" value="{{old('rightMove', $property->rightMove)}}" placeholder="Rightmove URL">
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="form-gorup">
                        <button type="submit" class="btn btn-primary">Update Home</button>
                    </div>
                </div>
            </div>
        </form>

        @if (count($errors) > 0)
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

@stop