@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="page-heading">Pending Reviews</h2>
    </div>
            @forelse($reviews->chunk(4) as $chunk)
                <div class="row full-width-white">
                    <div class="container">
                        @foreach($chunk as $review)
                                <div class="property-box col-md-3">
                                    <p class="property-box-price">
                                        {{$review->review}}
                                    </p>

                                    <a href="{{route('review.accept', ['id' => $review->id])}}">
                                        <div class="tick"></div>
                                    </a>
                                    <form action="{{route('review.destroy', ['id' => $review->id])}}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" name="submit" class="cross">
                                        <!--<div class="cross"></div>-->
                                        </button>
                                    </form>
                                </div>
                        @endforeach
                    </div>
                </div>
            @empty
                <div class="container">
                    <h2>There are no pending reviews</h2>
                </div>
            @endforelse
        </div>
    </div>
@stop