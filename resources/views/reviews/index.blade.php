@extends('layouts.app')

@section('content')

    <div class="container">
        <div style="height: 390px;">
            <a href="{{route('review.create')}}" class="btn btn-warning pull-right">Submit Review</a>
            <h2 class="page-heading">Customer reviews.</h2>
        </div>
        @foreach($reviews as $review)
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if(auth()->check())
                        <a href="{{route('review.edit', ['id' => $review->id])}}"></a>
                        <form action="{{route('review.destroy', ['id' => $review->id])}}" method="post">
                            {!! csrf_field() !!}
                            <input type="hidden" name="_method" value="DELETE">
                            <button class="pull-right delete-button" type="submit">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                        </form>
                    @endif
                    <h3 class="panel-title">Review By {{$review->name}}</h3>
                </div>
                <div class="panel-body reviews">
                    <p>{{$review->review}}</p>
                    <ul class="stars">
                        <li @if($review->ratings()->first()->rating > 0) class="checked" @endif></li>
                        <li @if($review->ratings()->first()->rating > 1) class="checked" @endif></li>
                        <li @if($review->ratings()->first()->rating > 2) class="checked" @endif></li>
                        <li @if($review->ratings()->first()->rating > 3) class="checked" @endif></li>
                        <li @if($review->ratings()->first()->rating > 4) class="checked" @endif></li>
                    </ul>
                </div>
            </div>
        @endforeach
    </div>

@stop

