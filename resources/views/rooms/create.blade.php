@extends('layouts.app')

@section('content')

    <div class="container">
        <h1 class="page-heading">Add Room Names to the house</h1>

        <form action="{{route('rooms.store', ['id' => $property->id])}}" method="post">
            <div class="row">
                <div class="col-md-6">
                    {!! csrf_field() !!}
                    <div class="form-group row">
                        <label for="number">Room 1: </label>
                        <input type="text" name="room[]" id="room[]" class="form-control" value="{{old("room[]")}}" required placeholder="Name">
                        <textarea name="desc[]" class="form-control" placeholder="Description"></textarea>
                        <input type="text" class="col-md-6" name="width[]" placeholder="Width"> <input type="text" class="col-lg-6" name="height[]" placeholder="Length">
                        <select name="unit[]">
                            <option value=""></option>
                            <option value="Meters">Meters</option>
                            <option value="Centimeters">Centimeters</option>
                            <option value="Millimeters">Millimeters</option>
                            <option value="Feet">Feet</option>
                            <option value="Inches">Inches</option>
                        </select>
                    </div>
                    <div class="form-group row">
                        <label for="number">Room 2: </label>
                        <input type="text" name="room[]" id="room[]" class="form-control" value="{{old("room[]")}}" placeholder="Name">
                        <textarea name="desc[]" class="form-control" placeholder="Description"></textarea>
                        <input type="text" class="col-md-6" name="width[]" placeholder="Width"> <input type="text" class="col-lg-6" name="height[]" placeholder="Length">
                        <select name="unit[]">
                            <option value=""></option>
                            <option value="Meters">Meters</option>
                            <option value="Centimeters">Centimeters</option>
                            <option value="Millimeters">Millimeters</option>
                            <option value="Feet">Feet</option>
                            <option value="Inches">Inches</option>
                        </select>
                    </div>
                    <div class="form-group row">
                        <label for="number">Room 3: </label>
                        <input type="text" name="room[]" id="room[]" class="form-control" value="{{old("room[]")}}" placeholder="Name">
                        <textarea name="desc[]" class="form-control" placeholder="Description"></textarea>
                        <input type="text" class="col-md-6" name="width[]" placeholder="Width"> <input type="text" class="col-lg-6" name="height[]" placeholder="Length">
                        <select name="unit[]">
                            <option value=""></option>
                            <option value="Meters">Meters</option>
                            <option value="Centimeters">Centimeters</option>
                            <option value="Millimeters">Millimeters</option>
                            <option value="Feet">Feet</option>
                            <option value="Inches">Inches</option>
                        </select>
                    </div>
                    <div class="form-group row">
                        <label for="number">Room 4: </label>
                        <input type="text" name="room[]" id="room[]" class="form-control" value="{{old("room[]")}}" placeholder="Name">
                        <textarea name="desc[]" class="form-control" placeholder="Description"></textarea>
                        <input type="text" class="col-md-6" name="width[]" placeholder="Width"> <input type="text" class="col-lg-6" name="height[]" placeholder="Length">
                        <select name="unit[]">
                            <option value=""></option>
                            <option value="Meters">Meters</option>
                            <option value="Centimeters">Centimeters</option>
                            <option value="Millimeters">Millimeters</option>
                            <option value="Feet">Feet</option>
                            <option value="Inches">Inches</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="number">Room 5: </label>
                        <input type="text" name="room[]" id="room[]" class="form-control" value="{{old("room[]")}}" placeholder="Name">
                        <textarea name="desc[]" class="form-control" placeholder="Description"></textarea>
                        <input type="text" class="col-md-6" name="width[]" placeholder="Width"> <input type="text" class="col-lg-6" name="height[]" placeholder="Length">
                        <select name="unit[]">
                            <option value=""></option>
                            <option value="Meters">Meters</option>
                            <option value="Centimeters">Centimeters</option>
                            <option value="Millimeters">Millimeters</option>
                            <option value="Feet">Feet</option>
                            <option value="Inches">Inches</option>
                        </select>
                    </div>
                    <div class="form-group row">
                        <label for="number">Room 6: </label>
                        <input type="text" name="room[]" id="room[]" class="form-control" value="{{old("room[]")}}" placeholder="Name">
                        <textarea name="desc[]" class="form-control" placeholder="Description"></textarea>
                        <input type="text" class="col-md-6" name="width[]" placeholder="Width"> <input type="text" class="col-lg-6" name="height[]" placeholder="Length">
                        <select name="unit[]">
                            <option value=""></option>
                            <option value="Meters">Meters</option>
                            <option value="Centimeters">Centimeters</option>
                            <option value="Millimeters">Millimeters</option>
                            <option value="Feet">Feet</option>
                            <option value="Inches">Inches</option>
                        </select>
                    </div>
                    <div class="form-group row">
                        <label for="number">Room 7: </label>
                        <input type="text" name="room[]" id="room[]" class="form-control" value="{{old("room[]")}}" placeholder="Name">
                        <textarea name="desc[]" class="form-control" placeholder="Description"></textarea>
                        <input type="text" class="col-md-6" name="width[]" placeholder="Width"> <input type="text" class="col-lg-6" name="height[]" placeholder="Length">
                        <select name="unit[]">
                            <option value=""></option>
                            <option value="Meters">Meters</option>
                            <option value="Centimeters">Centimeters</option>
                            <option value="Millimeters">Millimeters</option>
                            <option value="Feet">Feet</option>
                            <option value="Inches">Inches</option>
                        </select>
                    </div>
                    <div class="form-group row">
                        <label for="number">Room 8: </label>
                        <input type="text" name="room[]" id="room[]" class="form-control" value="{{old("room[]")}}" placeholder="Name">
                        <textarea name="desc[]" class="form-control" placeholder="Description"></textarea>
                        <input type="text" class="col-md-6" name="width[]" placeholder="Width"> <input type="text" class="col-lg-6" name="height[]" placeholder="Length">
                        <select name="unit[]">
                            <option value=""></option>
                            <option value="Meters">Meters</option>
                            <option value="Centimeters">Centimeters</option>
                            <option value="Millimeters">Millimeters</option>
                            <option value="Feet">Feet</option>
                            <option value="Inches">Inches</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-gorup">
                        <button type="submit" class="btn btn-primary">Add rooms</button>
                    </div>
                </div>
            </div>
        </form>

        @if (count($errors) > 0)
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

@stop