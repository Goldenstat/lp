@extends('layouts.app')

@section('content')
    <div class="container search">
        <h2 class="page-heading">Search</h2>
        <form action="{{route('search')}}" method="post">
            {!! csrf_field() !!}
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="min_price">Minimum Price: </label>
                    <select name="min_price" id="min_price" class="form-control" required>
                        <option value="0">0</option>
                        <option value="50000">50k</option>
                        <option value="100000">100k</option>
                        <option value="120000">120k</option>
                        <option value="140000">140k</option>
                        <option value="160000">160k</option>
                        <option value="180000">180k</option>
                        <option value="200000">200k</option>
                        <option value="250000">250k</option>
                        <option value="300000">300k</option>
                        <option value="400000">300k+</option>
                    </select>
                    <!--<input type="text" id="price" data-slider-ticks="[0, 50, 100, 120, 140, 160, 180, 200, 300]" data-slider-ticks-snap-bounds="30" data-slider-ticks-labels="['£0','£50k', '£100k', '£120k', '£140k', '£160k', '£180k', '£200k', '£300k+']">-->
                </div>
                <div class="form-group col-sm-6">
                    <label for="max_price">Maximum Price: </label>
                    <select name="max_price" id="max_price" class="form-control" required>
                        <option value="0">No Max Price</option>
                        <option value="50000">50k</option>
                        <option value="100000">100k</option>
                        <option value="120000">120k</option>
                        <option value="140000">140k</option>
                        <option value="160000">160k</option>
                        <option value="180000">180k</option>
                        <option value="200000">200k</option>
                        <option value="250000">250k</option>
                        <option value="300000">300k</option>
                        <option value="400000">300k+</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="min_bedrooms">Minimum Bedrooms: </label>
                    <select name="min_bedrooms" id="min_bedrooms" class="form-control" required>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5+</option>
                    </select>
                </div>
                <div class="form-group col-sm-6">
                    <label for="max_bedrooms">Maximum Bedrooms: </label>
                    <select name="max_bedrooms" id="max_bedrooms" class="form-control" required>
                        <option value="0">No Maximum bedrooms</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5+</option>
                    </select>
                </div>
            </div>
            <div class="form-gorup">
                <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
            </div>
        </form>
    </div>
@stop

@section('scripts')
    <!--<script>
        $('#price').slider({
            ticks: [0, 50, 100, 120, 140, 160, 180, 200, 300],
            ticks_labels: ['£0','£50k', '£100k', '£120k', '£140k', '£160k', '£180k', '£200k', '£300k+'],
            ticks_snap_bounds: 30
        });
    </script>-->
@stop