@extends('layouts.app')

@section('content')
    <div class="container contact">
        <h2 class="page-heading">Contact us</h2>
        <form class="form-horizontal" method="post" action="/contact">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="firstname" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Your first name">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <label for="message" class="col-sm-2 control-label">Message: </label>
                <div class="col-sm-10">
                    <textarea name="message" id="message" class="form-control" rows="5" placeholder="Your message" required>
                        {{old('message')}}
                    </textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Contact us</button>
                </div>
            </div>
        </form>
    </div>
@stop