
@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="page-heading">Why choose Leverson Palmer</h2>
        
        <ul class="faq">
            <li>Q: "You charge a lot less than other estate agents, why is that? It sounds too good to be true!"</li>
            <li>A: High street agents charge huge fees because they have huge overheads, fleets of luxury cars and expensive shop rents. Being your local online estate agent, we don't have those overheads here at Leverson Palmer so we can pass those savings onto you and still spend the same on marketing!</li>
            <li>Q: "Does a fixed fee mean you don't have the incentive to achieve a higher price?"</li>
            <li>A: On an average commission of 2%, every extra £1000 the agent gets for you will only net them £2. At
                Leverson Palmer, it is our legal obligation to achieve the best price for you and a huge amount of our
                business comes from recommendations so we want you to be happy so you can continue to spread the word!
            </li>
            <li>Q: "Some agents claim to have more buyers who will offer more money?"</li>
            <li>A: Those days are long gone! The internet has levelled the field. Buyers can search all properties from
                all agents quickly and easily whether they are on the agents database or not!
            </li>
            <li>Q: " Will you look after the sale for me from start to finish?"</li>
            <li>A: Absolutely! We will take the stress off your shoulders and look after everything for you, from
                instructing solicitors to arranging removals vans!
            </li>
            <li>Q: "Who would I be dealing with on a day to day basis?"</li>
            <li>A: You will not only be dealing with our highly trained sales staff, but the owner of the business is only a phone call away!</li>
        </ul>
    </div>
@stop