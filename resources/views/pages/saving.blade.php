@extends('layouts.app')

@section('content')
    <section class="carousel">
        <ul class="slides">
            <li><img src="http://www.harperfinn.co.uk/layout/our-fees.jpg" alt=""></li>
        </ul>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-md-8 subText">
                <h1 class="page-heading">Your Saving</h1>
                <h3>The Fixed Fee revision</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis corporis earum ex expedita molestias, nulla odio saepe. Aspernatur explicabo ipsa iste obcaecati voluptate? A eius magnam natus nisi quasi repellendus.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda commodi consectetur corporis culpa debitis, ea earum eius eveniet facere ipsum iste maiores nam officia praesentium quasi repellat totam ullam vero.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores commodi consequuntur distinctio exercitationem fugiat illum itaque, placeat quibusdam reprehenderit? Adipisci deleniti facilis labore minima modi molestias obcaecati sint, sunt voluptatibus.</p>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Property Value</th>
                            <th>Conventional estate agent fee</th>
                            <th>Leverson Palmer fixed rate</th>
                            <th>You save</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>£500k</td>
                        <td>£10,000</td>
                        <td>£7,999</td>
                        <td>£2,000</td>
                    </tr>
                    <tr>
                        <td>£1 million</td>
                        <td>£20,000</td>
                        <td>£7,999</td>
                        <td>£12,000</td>
                    </tr>
                    <tr>
                        <td>£1.5 million</td>
                        <td>£30,000</td>
                        <td>£7,999</td>
                        <td>£22,000</td>
                    </tr>
                    <tr>
                        <td>£2 million</td>
                        <td>£40,000</td>
                        <td>£7,999</td>
                        <td>£32,000</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop