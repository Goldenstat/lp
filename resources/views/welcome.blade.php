@extends('layouts.app')

@section('content')
<section class="carousel">
    <ul class="slides">
        <li>
            <!--<img src="http://placehold.it/1560x520" alt="">-->
            <img src="img/bannercopy.png" alt="">
        </li>
    </ul>
</section>
<div class="container-fluid home">
    <div class="row">
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-body main">
                    <h2>Why choose Leverson Palmer?</h2>
                    <p>Leverson Palmer Estate Agents is a property sales agency specialising in Loughborough and surrounding areas. We don't have fancy premises and you won't find us cluttering up the high street. Yet over the last few years, more and more people have chosen to sell their property through an online agent due to the amount of money you can save! We are an estate agency with a difference!</p>

                    <p>The difference is that we offer the same, if not better service as the high street agents. But at a much lower cost. Instead of charging a percentage, we charge a small upfront fixed fee. We think that's fairer and East Midlands property owners seem to agree.</p>

                    <p>Want to find out more? Call us today discover why Leverson Palmer is a smarter way to sell your property.</p>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Connect with us</h3>
                        </div>
                        <div class="panel-body social">
                            <div class="row">
                                <div class="col-md-6"><a href="https://www.facebook.com/leversonpalmer/"><i class="fa fa-facebook-square fa-lg">Like us on Facebook</i></a></div>
                                <div class="col-md-6"><a href="https://twitter.com/leversonpalmer"><i class="fa fa-twitter-square fa-lg">Follow us on Twitter</i></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Latest News</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="news">
                                <li><a href="#">Link to news item 1</a></li>
                                <li><a href="#">Link to news item 2</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
    <div class="latest-nav">
        <ul>
            <li class="properties-head"><span>Latest Properties</span></li>
        </ul>
    </div>
</div>
<div class="row full-width-white">
    <div class="container-fluid">
        @forelse($properties as $property)
            <div class="property-box col-md-3">
                <a href="/properties/{{$property->id}}" class="center-block">
                    <div class="bg-fade">
                        <img src="{!! $property->photos->first()['path'] !!}" alt="" class="fade-img property-img">
                    </div>
                    <p class="property-box-rd">{{$property->street}} {{$property->post}}</p>
                    <p class="property-box-price">&pound; {{number_format($property->price) }}</p>
                </a>
                <a href="#" class="btn btn-sm btn-default">{{$property->state}}</a>
                <a href="{{$property->right_move}}" class="btn btn-primary pull-right">Right Move</a>
            </div>
        @empty
            <div class="property-box col-md-12">
                There are currently no properties available.
            </div>
        @endforelse
            <div class="property-box col-md-3 home-search">
                <h3 class="text-center">Search</h3>
                <form action="{{route('search')}}" method="post" class="form-horizontal">
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="min_price">Minimum Price: </label>
                            <select name="min_price" id="min_price" class="form-control" required>
                                <option value="0">0</option>
                                <option value="50000">50k</option>
                                <option value="100000">100k</option>
                                <option value="120000">120k</option>
                                <option value="140000">140k</option>
                                <option value="160000">160k</option>
                                <option value="180000">180k</option>
                                <option value="200000">200k</option>
                                <option value="250000">250k</option>
                                <option value="300000">300k</option>
                                <option value="400000">300k+</option>
                            </select>
                            <!--<input type="text" id="price" data-slider-ticks="[0, 50, 100, 120, 140, 160, 180, 200, 300]" data-slider-ticks-snap-bounds="30" data-slider-ticks-labels="['£0','£50k', '£100k', '£120k', '£140k', '£160k', '£180k', '£200k', '£300k+']">-->
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="max_price">Maximum Price: </label>
                            <select name="max_price" id="max_price" class="form-control" required>
                                <option value="0">No Max Price</option>
                                <option value="50000">50k</option>
                                <option value="100000">100k</option>
                                <option value="120000">120k</option>
                                <option value="140000">140k</option>
                                <option value="160000">160k</option>
                                <option value="180000">180k</option>
                                <option value="200000">200k</option>
                                <option value="250000">250k</option>
                                <option value="300000">300k</option>
                                <option value="400000">300k+</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="min_bedrooms">Minimum Bedrooms: </label>
                            <select name="min_bedrooms" id="min_bedrooms" class="form-control" required>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5+</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="max_bedrooms">Maximum Bedrooms: </label>
                            <select name="max_bedrooms" id="max_bedrooms" class="form-control" required>
                                <option value="0">No Maximum bedrooms</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5+</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-gorup">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
                    </div>
                </form>
            </div>
    </div>
</div>
@endsection
