@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="page-heading">
            Admin Controls
        </h1>
        <div class="panel panel-default">
            <div class="panel-body">
                <ul>
                    <li><a href="{{route('properties.create')}}">Add a property</a></li>
                    <li><a href="{{route('review.pending')}}">Evaluate pending reviews {{$reviews}}</a></li>
                </ul>
            </div>
        </div>
    </div>
@stop