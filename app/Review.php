<?php

namespace App;

use Ghanem\Rating\Contracts\Ratingable;
use Ghanem\Rating\Traits\Ratingable as RatingTrait;
use Illuminate\Database\Eloquent\Model;

class Review extends Model implements Ratingable
{
    use RatingTrait;

    protected $fillable = [
        'name',
        'email',
        'review',
        'live'
    ];
}
