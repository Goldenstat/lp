<?php

namespace App\Listeners;

use App\Events\PropertyAdded;
use App\Property;
use Frozensheep\RightmoveADF\Values\Parkings;
use Frozensheep\RightmoveADF\Values\PropertyTypes;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class PushToADF
{
    use SerializesModels;

    public $property;

    /**
     * Create the event listener.
     *
     * @param Property $property
     */
    public function __construct(Property $property)
    {
        $this->property = $property;
    }

    /**
     * Handle the event.
     *
     * @param  PropertyAdded  $event
     * @return void
     */
    public function handle(PropertyAdded $event)
    {
        $objRightmoveADF = new RightmoveADF(env('ADF_CERT'), env('ADF_CERT_PASS'), RightmoveADF::TEST);

        $objRequest = $objRightmoveADF->createRequest(RightmoveADF::GetBranchPropertyList);

        $objRequest->network->network_id = env('ADF_NETWORK_ID');
        $objRequest->branch->branch_id = env('ADF_BRANCH_ID');
        $objRequest->branch->channel = Channels::Sales;
        $objRequest->property->agent_ref = env('ADF_PROPERTY_REF');

        $objRequest->property->published = true;
        $objRequest->property->property_type = PropertyTypes::DetachedHouse;
        PropertyTypes::Fla
        $objRequest->property->status = Statuses::Available;

        $number = $objRequest->property->address->house_name_number = '43';
        $street = $objRequest->property->address->address_2 = 'The Banks';
        $town = $objRequest->property->address->town = 'Sileby';
        $post1 = $objRequest->property->address->postcode_1 = 'LE12';
        $post2 = $objRequest->property->address->postcode_2 = '7RE';
        $objRequest->property->address->display_address = $number . ' ' . $street . ' ' . $town . ' ' . $post1.$post2;
        $objRequest->property->price_information->price = 200;

        $objRequest->property->details->summary = 'Just a summary';
        $objRequest->property->details->description = 'Full Description';
        $objRequest->property->details->bedrooms = 2;
        $objRequest->property->details->reception_rooms = 2;

        $objMedia1 = $objRequest->property->media()->create();
        $objMedia1->media_type = MediaTypes::Image;
        $objMedia1
    }
}
