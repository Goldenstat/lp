<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Photo;
use App\Property;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class PagesController extends Controller
{


    /**
     * PagesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['admin']]);
    }

    public function index()
    {
        $properties = Property::with('photos')->orderBy('created_at', 'desc')->take(3)->get();

        return view('welcome', compact('properties'));
    }
    public function saving()
    {
        return view('pages.saving');
    }

    public function destroyPhoto($id)
    {
        $photo = Photo::findOrFail($id);
        Storage::delete([$photo->path, $photo->thumbnail_path]);
        $photo->delete();

        return back();
    }

    public function admin()
    {
        $reviews = Review::where('live','no')->count();

        return view('admin', compact('reviews'));
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function sendMail(Request $request)
    {
        $data = $request->all();

        Mail::send('mail.contact', ['data' => $data], function($m) use ($data) {
            $m->from('postmaster@leversonpalmer.co.uk', 'Postmaster');
            $m->to('rpalmer@leversonpalmer.com');
            $m->subject('Message from the Leverson Palmer website.');
        });

        flash()->success('Success', 'Your message has been sent. Thank you.');

        return redirect()->refresh();
    }

    public function about()
    {
        return view('pages.about');
    }

    public function search()
    {
        return view('pages.search');
    }

    public function postSearch(SearchRequest $request)
    {
        $min_price = $request->get('min_price');
        $max_price = $request->get('max_price');
        $min_bedrooms = $request->get('min_bedrooms');
        $max_bedrooms = $request->get('max_bedrooms');

        if($max_price > 0 && $max_bedrooms < 1) {
            $properties = Property::with('photos')->where('price','>=',$min_price)->where('price', '<=', $max_price)->get();
        } elseif($max_price > 0 && $max_bedrooms > 0) {
            $properties = Property::with('photos')->where('price','>=',$min_price)->where('price', '<=', $max_price)->where('bedrooms', '<=', $max_bedrooms)->get();
        } elseif($max_bedrooms > 0 && $max_price < 1) {
            $properties = Property::with('photos')->where('bedrooms', '>=', $min_bedrooms)->where('bedrooms', '<=', $max_bedrooms)->get();
        } else {
            $properties = Property::with('photos')->where('price','>',$request->get('min_price'))->get();
        }

        return view('properties.index', compact('properties'));
    }
}
