<?php

namespace App\Http\Controllers;

use App\Http\Requests\PropertyRequest;
use App\Photo;
use App\Property;
use App\Room;
use Frozensheep\RightmoveADF\RightmoveADF;
use Frozensheep\RightmoveADF\Values\Channels;
use Frozensheep\RightmoveADF\Values\MediaTypes;
use Frozensheep\RightmoveADF\Values\PropertyTypes;
use Frozensheep\RightmoveADF\Values\Statuses;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Image;

use App\Events\PropertyAdded;

class PropertiesController extends Controller
{
    /**
     * PropertiesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index','show']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $properties = Property::with('photos')->orderBy('created_at', 'desc')->get();

        return view('properties.index', compact('properties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('properties.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PropertyRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PropertyRequest $request)
    {

        $data = Property::create($request->all());

        //event(new PropertyAdded($data));

        flash()->success('Success', 'The property has been added.');

        return redirect()->route('rooms.create', ['id' => $data->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $property = Property::with('rooms')->findOrFail($id);

        return view('properties.show', compact('property'));
    }

    public function addPhoto($id, Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $file = $request->file('photo');

        $name = time() . $file->getClientOriginalName();
        $thumbnail_path = "img/properties/photos/tn-{$name}";

        $file->move('img/properties/photos', $name);

        $property = Property::findOrFail($id);

        $property->photos()->create(['name' => $name, 'path' => "img/properties/photos/{$name}", 'thumbnail_path' => $thumbnail_path]);

        Image::make("img/properties/photos/{$name}")->fit(200)->save($thumbnail_path);

        return 'done';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $property = Property::findOrFail($id);

        return view('properties.edit', compact('property'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Property::findOrFail($id);
        $data ->update($request->all());

        flash()->success('Success', 'You have successfully updated the property.');

        return redirect()->route('properties.show', ['id' => $data->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $rooms = Room::where('property_id', $id)->get();
        $photos = Photo::where('property_id', $id)->get();
        $property = Property::findOrFail($id);

        if($property->rightMove != '')
        {
            flash()->error('Error!', 'You must remove the property from rightmove before you delete it from the website.');
            return redirect()->route('properties.show', ['id' => $id]);
        }
        foreach($rooms as $room)
        {

            $room->delete();
        }

        foreach($photos as $photo)
        {
            Storage::delete([$photo->path, $photo->thumbnail_path]);
            $photo->delete();
        }

        $property->delete();

        flash()->success('Success', 'You have removed that property.');

        return redirect()->route('properties.index');
    }

    public function rightMove($id)
    {
        $property = Property::with('photos', 'rooms')->findOrFail($id);

        $objRightmoveADF = new RightmoveADF(app_path(env('ADF_CERT')), env('ADF_CERT_PASS'), RightmoveADF::TEST);

        $objRequest = $objRightmoveADF->createRequest(RightmoveADF::SendProperty);


        $objRequest->network->network_id = intval(env('ADF_NETWORK_ID'));
        $objRequest->branch->branch_id = intval(env('ADF_BRANCH_ID'));
        $objRequest->branch->channel = Channels::Sales;
        $objRequest->property->agent_ref = $id;

        $objRequest->property->published = true;
        $objRequest->property->property_type = $this->getPropertyTypes($property->type);
        $objRequest->property->status = $this->getStatus($property->state);

        $number = $objRequest->property->address->house_name_number = (string)$property->number;
        $street = $objRequest->property->address->address_2 = $property->street;
        $town = $objRequest->property->address->town = $property->town;
        $post1 = $objRequest->property->address->postcode_1 = $property->code1;
        $post2 = $objRequest->property->address->postcode_2 = $property->code2;
        $objRequest->property->address->display_address = $number . ' ' . $street . ' ' . $town . ' ' . $post1. ' ' .$post2;
        $objRequest->property->price_information->price = $property->price;

        $objRequest->property->details->summary = substr($property->description, 0, 100);
        $objRequest->property->details->description = $property->description;
        $objRequest->property->details->bedrooms = (int)$property->bedrooms;
        $objRequest->property->details->reception_rooms = (int)$property->reception;
        $objRequest->property->details->parking()[] = $this->getParkingTypes($property->parking);


        $i = 0;
        $j = 0;
        $room = null;
        $media = null;
        foreach ($property->rooms as $roomobj) {
            $i++;
            ${'room' . $i} = $objRequest->property->details->rooms()->create();
            ${'room' . $i}->room_name = $roomobj->name;
            ${'room' . $i}->room_description = $roomobj->desc;
            ${'room' . $i}->room_length = $roomobj->length;
            ${'room' . $i}->room_width = $roomobj->width;
            ${'room' . $i}->room_dimension_unit = $this->getUnits($roomobj->unit);
        }

        foreach($property->photos as $photo)
        {
            $j++;
            ${'media' . $j} = $objRequest->property->media()->create();
            ${'media' . $j}->media_type = MediaTypes::Image;
            $photoUrl = 'http://leversonpalmer.co.uk/'.$photo->path;
            ${'media' . $j}->media_url = $photoUrl;
        }

        $objRequest->property->principal->principal_email_address = 'rpalmer@leversonpalmer.com';
        $objRequest->property->principal->auto_email_when_live = true;
        $objRequest->property->principal->auto_email_updates = true;


        $objResponse = $objRightmoveADF->send($objRequest);

        if($objResponse->success) {
            $property->rightMove = $objResponse->property->rightmove_url;
            $property->save();
            flash()->success('Success', 'Property has been added to RightMove!');
            return redirect()->route('properties.show', ['id' => $id]);
        } else {
            print_r($objResponse->errors);
        }

    }

    public function removeRightMove($id)
    {
        $property = Property::findOrFail($id);
        $objRightmoveADF = new RightmoveADF(app_path(env('ADF_CERT')), env('ADF_CERT_PASS'), RightmoveADF::TEST);

        $objRequest = $objRightmoveADF->createRequest(RightmoveADF::RemoveProperty);

        $objRequest->network->network_id = intval(env('ADF_NETWORK_ID'));
        $objRequest->branch->branch_id = intval(env('ADF_BRANCH_ID'));
        $objRequest->branch->channel = Channels::Sales;
        $objRequest->property->agent_ref = $id;

        $objResponse = $objRightmoveADF->send($objRequest);

        if($objResponse->success) {
            $property->rightMove = '';
            $property->save();
            flash()->success('Success', 'The property has successfully been removed from rightmove.');
            return redirect()->route('properties.show', ['id' => $id]);
        } else {
            print_r($objResponse->errors);
        }
    }

    private function getPropertyTypes($type)
    {
        switch($type) {
            case 'DetachedHouse':
                return 4;
            break;
            case 'SemiDetachedHouse':
                return 3;
            break;
            case 'TerracedHouse':
                return 1;
            break;
            case 'Flat':
                return 8;
            break;
        }
    }

    private function getParkingTypes($type)
    {
        switch($type) {
            case 'Allocated':
                return 13;
            break;
            case 'Communal':
                return 14;
            break;
            case 'Covered':
                return 15;
            break;
            case 'Garage':
                return 16;
            break;
            case 'Driveway':
                return 17;
            break;
            case 'Gated':
                return 18;
            break;
            case 'OffStreet':
                return 19;
            break;
            case 'Onstreet':
                return 20;
            break;
            case 'Rear':
                return 21;
            break;
            case 'Permit':
                return 22;
            break;
            case 'PrivateParking':
                return 23;
            break;
            case 'Residents':
                return 24;
            break;
        }
    }

    private function getStatus($status)
    {
        switch($status){
            case 'Available':
                return 1;
            break;
            case 'UnderOffer':
                return 4;
            break;
        }
    }

    private function getUnits($unit)
    {
        switch($unit){
            case 'Metres':
                return 5;
            break;
            case 'Centimetres':
                return 6;
            break;
            case 'Millimetres':
                return 7;
            break;
            case 'Feet':
                return 8;
            break;
            case 'Inches':
                return 9;
            break;
        }
    }

    public function rightmoveProperties()
    {
        $objRightmoveADF = new RightmoveADF(app_path(env('ADF_CERT')), env('ADF_CERT_PASS'), RightmoveADF::TEST);

        $objRequest = $objRightmoveADF->createRequest(RightmoveADF::GetBranchPropertyList);

        $objRequest->network->network_id = intval(env('ADF_NETWORK_ID'));
        $objRequest->branch->branch_id = intval(env('ADF_BRANCH_ID'));

        $objResponse = $objRightmoveADF->send($objRequest);

        print_r($objResponse);
    }
}
