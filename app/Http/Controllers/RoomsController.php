<?php

namespace App\Http\Controllers;

use App\Property;
use App\Room;
use Illuminate\Http\Request;

use App\Http\Requests;

class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $property = Property::findOrFail($id);
        return view('rooms.create', compact('property'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        $name = $request->get('room');
        $desc = $request->get('desc');
        $width = $request->get('width');
        $height = $request->get('height');
        $unit = $request->get('unit');

        for($i = 0; $i < sizeof($name); $i++)
        {
            if(strlen($name[$i]) < 1 || strlen($desc[$i]) < 1 || strlen($width[$i]) < 1 || strlen($height[$i]) < 1 || strlen($unit[$i]) < 1)
            {
                continue;
            }
            Room::create(['name' => $name[$i], 'desc' => $desc[$i], 'length' => $height[$i], 'width' => $width[$i], 'unit' => $unit[$i], 'property_id' => $id]);
        }

        flash()->success('Success', 'The Rooms have been added to your property.');

        return redirect()->route('properties.show', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
