<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReviewRequest;
use App\Review;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFound;

use App\Http\Requests;

class ReviewsController extends Controller
{
    /**
     * ReviewsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'create', 'store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Review::where('live','yes')->get();

        return view('reviews.index', compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateReviewRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateReviewRequest $request)
    {
        $data = $request->except('rating');
        $data += ['live' => 'no'];

        $review = Review::create($data);

        $user = User::first();

        $rating = $review->rating(['rating' => $request->get('rating')], $user);


        flash()->success('Success', 'You have submitted a review.');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review = Review::destroy($id);

        flash()->success('Success!', 'You have deleted that review');

        return redirect()->back();
    }

    public function pending()
    {
        $reviews = Review::where('live','no')->get();

        return view('reviews.pending', compact('reviews'));
    }

    public function accept($id)
    {
        $review = Review::where('id',$id)->where('live', 'no')->update(['live' => 'yes']);


        flash()->success('Success', 'You have made that review live!');
        return redirect()->back();
    }
}
