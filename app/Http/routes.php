<?php

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    /*Route::any('register', function() {
        return redirect()->route('index');
    });*/

    Route::get('/', ['as' => 'index', 'uses' => 'PagesController@index']);
    Route::get('/saving', 'PagesController@saving');
    Route::get('/admin', 'PagesController@admin');
    Route::get('/contact', 'PagesController@contact');
    Route::post('/contact', 'PagesController@sendMail');
    Route::get('/about', 'PagesController@about');
    Route::get('/search', 'PagesController@search');
    Route::post('/search', ['as' => 'search', 'uses' => 'PagesController@postSearch']);

    Route::resource('properties', 'PropertiesController');

    Route::post('properties/{id}/photos', 'PropertiesController@addPhoto');

    Route::get('properties/{id}/rightMove', ['as' => 'properties.rm', 'uses' => 'PropertiesController@rightMove']);
    Route::get('properties/{id}/removeRightMove', ['as' => 'properties.rrm', 'uses' => 'PropertiesController@removeRightMove']);
    Route::get('
    RightMove', ['as' => 'properties.rmp', 'uses' => 'PropertiesController@rightmoveProperties']);

    Route::delete('photos/{id}', 'PagesController@destroyPhoto');

    Route::get('review/pending', ['as' => 'review.pending', 'uses' => 'ReviewsController@pending']);
    Route::get('review/{id}/accept', ['as' => 'review.accept', 'uses' => 'ReviewsController@accept']);
    Route::resource('review', 'ReviewsController');
   Route::get('rooms/{id}/create', ['as' => 'rooms.create', 'uses' => 'RoomsController@create']);
   Route::post('rooms/{id}/create', ['as' => 'rooms.store', 'uses' => 'RoomsController@store']);
});
