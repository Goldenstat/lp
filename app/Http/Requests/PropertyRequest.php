<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PropertyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required|integer',
            'street' => 'required',
            'town' => 'required',
            'code1' => 'required',
            'code2' => 'required',
            'bedrooms' => 'required|integer',
            'reception' => 'required|integer',
            'gardens' => 'required|integer',
            'parking' => 'required',
            'type' => 'required',
            'price' => 'required|integer',
            'description' => 'required|min:50',
            'state' => 'required'
        ];
    }
}
