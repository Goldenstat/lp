<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'name',
        'desc',
        'width',
        'length',
        'unit',
        'property_id'
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }
}
