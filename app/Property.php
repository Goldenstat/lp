<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{

    protected $fillable = [
        'number',
        'street',
        'town',
        'code1',
        'code2',
        'bedrooms',
        'reception',
        'gardens',
        'parking',
        'type',
        'description',
        'price',
        'state',
        'rightMove'
    ];

    public function scopeLocatedAt($query, $number, $street, $city)
    {
        $street = str_replace('-', ' ', $street);

        return $query->where(compact($number, $street, $city))->first();

    }

    /*public function getPriceAttribute($price)
    {
        return '&pound;'. number_format($price);
    }*/

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }
}
