<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use File;
class Photo extends Model
{
    protected $fillable = ['name','path','thumbnail_path'];


    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function delete()
    {
        File::delete([
            $this->path,
            $this->thumbnail_path
        ]);

        parent::delete();
    }
}
