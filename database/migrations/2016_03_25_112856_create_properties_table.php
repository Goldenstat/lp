<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number');
            $table->string('street');
            $table->string('town');
            $table->string('code1');
            $table->string('code2');
            $table->integer('bedrooms');
            $table->integer('reception');
            $table->integer('gardens');
            $table->string('parking');
            $table->string('type');
            $table->integer('price');
            $table->text('description');
            $table->string('state');
            $table->string('rightMove');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
