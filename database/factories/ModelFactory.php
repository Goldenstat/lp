<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Property::class, function (Faker\Generator $faker) {
    return [
        'number' => $faker->biasedNumberBetween('1', '50'),
        'street' => $faker->streetName,
        'city' => $faker->city,
        'county' => 'Leicestershire',
        'post-code' => $faker->postcode,
        'bedrooms' => $faker->biasedNumberBetween('1', '4'),
        'reception' => $faker->biasedNumberBetween('1', '3'),
        'gardens' => $faker->biasedNumberBetween('1', '2'),
        'parking' => 'Yes there is allocated parking',
        'type' => 'house',
        'description' => $faker->paragraphs(1, true)
    ];
});
